﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FizzBuzz;

namespace FizzBuzz.App
{
    class Program
    {
        static void Main(string[] args)
        {
            var fbService = new FizzBuzzService();
            var numbers = fbService.GenerateNumberList(100);
            
	    foreach (var number in numbers)
            {
                Console.WriteLine(fbService.GetFizzBuzzValue(number));
            }

            Console.ReadKey();

        }
    }
}
