﻿using NUnit.Framework;
using System.Collections.Generic;

namespace FizzBuzz.Tests.Unit
{
    [TestFixture]
    public class WhenIterating
    {
        private FizzBuzzService _fizzBuzzService;

        [SetUp]
        public void SetUp()
        {
            _fizzBuzzService = new FizzBuzzService();
        }

        [Test]
        [TestCase(100)]
        [TestCase(1000)]
        [TestCase(2)]
        [TestCase(120)]
        [TestCase(250)]
        public void ItShouldGenerateListOfNumbersWithFlexibleCapacity(int max)
        {
            var listOfNumbers = _fizzBuzzService.GenerateNumberList(max);

            Assert.That(listOfNumbers.Capacity, Is.EqualTo(max));
            Assert.That(listOfNumbers, Is.InstanceOf<List<int>>());
            Assert.That(listOfNumbers.Contains(1));
            Assert.That(listOfNumbers.Contains(max));
        }

        [Test]
        public void ItShouldReturnFizzIfNumberIsDivisibleByThree()
        {
            const string expectedValue = "Fizz";
            const int num = 3;

            var value = _fizzBuzzService.GetFizzBuzzValue(num);
            Assert.That(value, Is.EqualTo(expectedValue));
        }

        [Test]
        public void ItshouldReturnBuzzIfNumberIsDivisibleByFive()
        {
            const string expectedValue = "Buzz";
            const int num = 5;

            var value = _fizzBuzzService.GetFizzBuzzValue(num);

            Assert.That(value, Is.EqualTo(expectedValue));
        }

        [Test]
        public void ItShouldReturnFizzBuzzIfNumberIsDivisibleByThreeAndFive()
        {
            const string expectedValue = "FizzBuzz";
            const int num = 15;

            var value = _fizzBuzzService.GetFizzBuzzValue(num);

            Assert.That(value, Is.EqualTo(expectedValue));
        }

        [Test]
        public void ItShouldReturnTheNumberIfNotDivisibleByThreeOrFive()
        {
            const string expectedValue = "4";
            const int num = 4;

            var value = _fizzBuzzService.GetFizzBuzzValue(num);

            Assert.That(value, Is.EqualTo(expectedValue));
        }
        
    }	
}

