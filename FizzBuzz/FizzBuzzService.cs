﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FizzBuzz
{
	public class FizzBuzzService
    {
        public List<int> GenerateNumberList(int limit)
        {
            var numberList = Enumerable.Range(1, limit).ToList();
            numberList.Capacity = limit;
            return numberList;
        }

        public string GetFizzBuzzValue(int num)
        {
            Func<int, bool> fizzBuzz = (x) => x % 5 == 0 && x % 3 == 0;
            Func<int, bool> fizz = (x) => x % 3 == 0;
            Func<int, bool> buzz = (x) => x % 5 == 0;

            return (fizzBuzz(num)) ? "FizzBuzz" :
                (fizz(num)) ? "Fizz" :
                (buzz(num)) ? "Buzz" :
                num.ToString();
        }
    }
}